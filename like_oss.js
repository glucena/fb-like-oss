var imgOn  = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAANCAYAAACdKY9CAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN8wAADfMBL/09/gAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAGiSURBVCiRfY9BSBRxGMXf95+Z/+7MOrMuYzuuQUoIKgh2DSI8BCsKKoSCxwiMDlJqBSqB4cUiksgK6phYHsSjeFEIT+JlTu5NVBSry66tLjo7/6/bMmn5bg/e73vfAyLqmtgazD71O8BM2SebXT0vDrI4Jz1qZCLtVLnOzN3pgq+qr/2I2Vf2AKxEMyJqlsbd10GpsGqYSY+VUsz4eWlD++SaHgalVlZhiybNm4Jw51LAPHE/xWszt0lorJvO8NdHtP5foOOZP2Z7TfdIaAzG0MJjen8+XNnQPrmmW6mGTqFJJuBh/tD3e6f27/8T6BzPvXJl25FuxJcBBOWzYi6WSFPcrpsdeMfNF4Cq6oYdaaV22ZBzzCpHhrUoZWoHwDcO8RHMFAX+Mv3TJ/0ibn6h8u+r+V/bfU5t64fgtPi8eLh1oFuOm6ixZ6myoZR+aVipdWklH5yVCjdiiRpPaEb03jFDfRYAYB7b9bbXNGwmM4OswiPTyXhCM0DAd1Lobs5DOw2K1wnUXXlp4C3fKofY1hBkSBijpDAzP0Ib0Yq+N9z4B4TKi3OZVQ7xAAAAAElFTkSuQmCC';
var imgOff = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAANCAYAAACdKY9CAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAOTQAADk0BRtVoCQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAF/SURBVCiRdZGxa5NRFMV/5733xUqoprWUbi4uCjo6CkZRaesQxGwO/gFOToJgBsEhTm5ioVOhi5HqJxra8g2FboK4CIogOGlcWtIWEt+7DjYSEj3TOXDvPefeKw5RFEXY6catJOU+xuXk/b2E3t5YrL5mCGFAOp2Oy47O3Be2FL2flDgZDM8I3IDU6/Ve7Xp1E/gmOIZp6pfTj9GGMCxa+fpp4CxwHqwbCV/+69BqbZwQ7iVQAfZBV+vzFzr/bGg0Gs6VtAacAn6a6WJtsfputBhAy0UxUT7wk1nqfwX2XLTLyXMOuYPaQvX5mEOlm95nqb9t8BRIqeR3DTxmz1rt9ux4JMcnjM8WsqbguMWUT7jeKvBRvfB4LNKweJFvPgGuVcruzM5eumvwyMxuObSbZNNTZb+ivxfKdAfpFaQloTmDubGNjYd//pDpJuKBQIZmB8UmPsis6SLr0du05N4EAJ+OrMTQK1nor7p+2Dfjijlr1uYvtSXZ4fzva/nG7d8CA5B5QzIRCAAAAABJRU5ErkJggg==';
//
function cssStringfy (obj) {
	return JSON.stringify(obj, null, '\t').replace(/\"/g,'').replace(/\,\s/g,';').replace(/\t/g,'')
}
//
var style = document.createElement('style');
var text = '';
//
var normal = {
	background: "url('"+imgOff+"')",
	'background-size': "100%",
	'background-position': "center !important",
	'background-repeat': "no-repeat"
}
var atctive = {
	background: "url('"+imgOn+"')",
	'background-size': "100%",
	'background-position': "center !important",
	'background-repeat': "no-repeat"
}
//
text += '.UFILikeLinkIcon '+cssStringfy(normal);
text += '\n';
text += '.UFILinkBright .UFILikeLinkIcon'+cssStringfy(atctive);
//
style.innerText = text;
document.head.appendChild(style)
//
function oss (){
	var likes = document.querySelectorAll('.UFILikeLink span , .UFICommentActions .UFILikeLink, .likeButton ')
	for (var i = likes.length - 1; i >= 0; i--) {
		if( likes.item(i).innerHTML.match(/(un)/gi) != null ){
			likes.item(i).innerHTML = 'Un-Oss'
		}else{
			likes.item(i).innerHTML = 'Oss'
		}
	};
}
//
window.addEventListener('scroll', oss);
oss();